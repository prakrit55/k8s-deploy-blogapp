FROM node:18 as builder

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and package-lock.json (or yarn.lock) files into the container
COPY package*.json ./

# Install the dependencies
RUN npm install react-scripts
RUN npm install

# Copy the rest of the application code into the container
COPY . .

COPY tsconfig.json tsconfig.json

# RUN npm run build
EXPOSE 3000


CMD ["npm", "start"]


# FROM nginx:stable-alpine

# RUN mkdir /usr/share/nginx/buffer

# COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
# COPY --from=builder app/public /usr/share/nginx/html

# RUN mkdir /usr/share/nginx/log

# EXPOSE 80

# CMD ["nginx", "-g", "daemon off;"]
